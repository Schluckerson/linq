﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Shop.Dto;
using Shop.Enums;
using Shop.Models;

namespace Shop
{
	public class ProductService
	{
		private readonly ShopContext _dbContext;

		private IList<Product> Products => _dbContext.Products.Include(p => p.Category).ToList();

		public ProductService(ShopContext dbContext)
		{
			_dbContext = dbContext;
		}

		/// <summary>
		/// Сортирует товары по цене и возвращает отсортированный список
		/// </summary>
		/// <param name="sortOrder">Порядок сортировки</param>
		public IEnumerable<Product> SortByPrice(SortOrder sortOrder)
		{
			if (sortOrder == SortOrder.Ascending)
            {
				return Products.OrderBy(Product => Product.Price);
            }
			else
            {
				return Products.OrderByDescending(Product => Product.Price);
            }
		}

		/// <summary>
		/// Возвращает товары, название которых начинается на <see cref="name"/>
		/// </summary>
		/// <param name="name">Фильтр - строка, с которой начинается название товара</param>
		public IEnumerable<Product> FilterByNameStart(string name)
		{
			return Products.Where(Product => Product.Name.Contains(name)); 
		}

		/// <summary>
		/// Группирует товары по производителю
		/// </summary>
		public IDictionary<string, List<Product>> GroupByVendor()
		{
			IDictionary<string, List<Product>> grouped = new SortedDictionary<string, List<Product>>();
			foreach(var v in Products.Select(x => x.Vendor).Distinct())
            {
				grouped.Add(v, Products.Where(x => x.Vendor == v).ToList());
            }				
			return grouped;
		}

		/// <summary>
		/// Возвращает список самых дорогих товаров (самые дорогие - товары с наибольшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheMostExpensiveProducts()
		{
			return Products.Where(Product => Product.Price == Products.Max(Product => Product.Price));
		}

		/// <summary>
		/// Возвращает список самых дешевых товаров (самые дешевые - товары с наименьшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheCheapestProducts()
		{			
			return Products.Where(Product => Product.Price == Products.Min(Product => Product.Price));
		}

		/// <summary>
		/// Возвращает среднюю цену среди всех товаров
		/// </summary>
		public decimal GetAverageProductPrice()
		{
			return Products.Average(Product => Product.Price);
		}

		/// <summary>
		/// Возвращает среднюю цену товаров в указанной категории
		/// </summary>
		/// <param name="categoryId">Идентификатор категории</param>
		public decimal GetAverageProductPriceInCategory(int categoryId)
		{
			return Products.Where(p => p.CategoryId == categoryId).Average(Product => Product.Price);
		}

		/// <summary>
		/// Возвращает список продуктов с актуальной ценой (после применения скидки)
		/// </summary>
		public IDictionary<Product, decimal> GetProductsWithActualPrice()
		{
			IDictionary<Product, decimal> result = new Dictionary<Product, decimal>();
			foreach (var p in Products.Select(p => { p.Price -= p.Price / 100 * p.Discount; return p; }).ToList())
            {
				result.Add(p, p.Price);
            }
			return result;
		}

		/// <summary>
		/// Возвращает список продуктов, сгруппированный по производителю, а внутри - по названию категории.
		/// Продукты внутри последней группы отсортированы в порядке убывания цены
		/// </summary>
		public IList<VendorProductsDto> GetGroupedByVendorAndCategoryProducts()
		{
			IList<VendorProductsDto> result = new List<VendorProductsDto>();
			VendorProductsDto item = new VendorProductsDto();
			foreach (var v in Products.Select(x => x.Vendor).Distinct())
            {
				item.Vendor = v;
				IDictionary<string, List<Product>> temp = new SortedDictionary<string, List<Product>>();
				List<Product> productsWithExactVendor = Products.Where(p => p.Vendor == v).ToList();
				foreach (var c in productsWithExactVendor.Select(x => x.Category).Distinct())
                {
					List<Product> productsWithExactCategory = new List<Product>();
					foreach (var p in productsWithExactVendor)
                    {
						if (p.Category == c) productsWithExactCategory.Add(p);
                    }
					temp.Add(c.Name, productsWithExactCategory.OrderByDescending(x => x.Price).ToList());
                }		
				item.CategoryProducts = temp;
				result.Add(item);
            }
			return result;
		}

		/// <summary>
		/// Обновляет скидку на товары, которые остались на складе в количестве 1 шт,
		/// и возвращает список обновленных товаров
		/// </summary>
		/// <param name="newDiscount">Новый процент скидки</param>
		public IEnumerable<Product> UpdateDiscountIfUnitsInStockEquals1AndGetUpdatedProducts(int newDiscount)
		{
			return Products.Where(x => x.UnitsInStock == 1).Select(x => { x.Discount = newDiscount; return x; });
		}
	}
}
